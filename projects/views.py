from django.shortcuts import redirect
from django.views.generic import ListView, DetailView, CreateView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "projectlist"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.save()
        return redirect("show_project", pk=item.id)
